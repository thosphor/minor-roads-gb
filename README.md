# minor-roads-gb

This is a project to filter the roads in Great Britain according to size or function, originally to 
identify low-traffic back roads most suitable for cycling.

## Using the html files

In the folders <whole_square_html> and <quarter_square_html> are html files which contain the 
locations of "Minor Roads" (and not A-roads, B-roads, Motoways, Restricted Local Access Roads etc.) 
laid over OpenStreetMap tiles.

The squares in question are those of the Ordnance Survey National Grid:

<https://getoutside.ordnancesurvey.co.uk/guides/beginners-guide-to-grid-references/>

These squares are 100 km x 100 km, so in cases where there are a lot of roads in that area, slow 
rendering times can make the html files unusable, so you can also use the squares split into 
quarters. The numbering system of the quarters is:

    2 | 1
    -----
    4 | 3


## Generating your own, applying your own filters

... To be added ..

<https://www.ordnancesurvey.co.uk/business-government/products/open-map-roads>
